<?php
    $args = array(
        "post_type" => 'post',
        "post_per_page" => 68,
        "orderby" => 'ID'
    );
    $p = new WP_Query($args);
    if ($p->have_posts()) {
?>
<div class="container">
    <div class="row">

<?php while ($p->have_posts()) {
             $p->the_post();
 ?>
        <div class="col-md-4">
           <h2><?php the_title(); ?></h2>
            <?php if (has_post_thumbnail()) { ?>
            <?php the_post_thumbnail(array(400, 300), array('class' => 'img-responsive')) ?>
            <?php } ?>
           
            <p><b><?= __('Posted on', 'sg') ?></b> <?php the_time('d.m.Y. H:i'); ?> </p>
           
            <?php
              $price = get_post_meta($post->ID, '_metaprice_data', true);
              $square = get_post_meta($post->ID, '_metasquare_data', true);
              $floor = get_post_meta($post->ID, '_metafloor_data', true);
            ?>
            <p class="price-button blue"><?php echo $price; ?></p>
            <p class="price-button blue">Площадь (общ./жил./кух.): <?php echo $square; ?></p>
            <p class="price-button blue">Этаж/этажность: <?php echo $floor; ?></p>
       
            <?php the_content(); ?>
        </div>
        <?php } } ?>
       <div class="col-md-3 pull-right">
           <?php get_sidebar() ?>
       </div>
    </div>
</div>