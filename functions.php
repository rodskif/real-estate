<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('realEstate', get_template_directory() . '/languages');
/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');
/**
 * Реєструємо місце для меню
 */
register_nav_menus( array(
    'primary' => __('Primary', 'realEstate'),
) );
/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'right',
        'name' => __('right sidebar', 'sg'),
        'description' => 'right sidebar with sale proporses',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ));

    register_sidebars(1, array(
        'id' => 'search-sidebar',
        'name' => __('Search sidebar', 'realEstate'),
        'description' => __('sidebar for search box', 'realEstate'),
        'class' => '',
        'before_widget' => '<div class="search-box">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebars(1, array(
        'id' => 'video-sidebar',
        'name' => __('Video sidebar', 'realEstate'),
        'description' => __('sidebar for video object', 'realEstate'),
        'class' => 'row',
        'before_widget' => '<div class="col-md-3 no-padding"><div class="widget-text footer-widget">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

    register_sidebars(1, array(
        'id' => 'baner-sidebar',
        'name' => __('Baner sidebar', 'realEstate'),
        'description' => __('sidebar for banner in home-page', 'realEstate'),
        'class' => 'row',
        'before_widget' => '<div class="col-md-4 no-padding">',
        'after_widget' => '</div>',
        'before_title' => '<div class="content-title-wrapper">
                        <div class="content-title-head">
                            <h3 class="content-title">',
        'after_title' => '</h3></div></div>'
    ));

    register_sidebars(1, array(
        'id' => 'sale-sidebar',
        'name' => __('Sale sidebar', 'realEstate'),
        'description' => __('sidebar for sale-box in right side', 'realEstate'),
        'class' => '',
        'before_widget' => '<div class="page-content-wrapper">
                <div class="container">',
        'after_widget' => '</div></div>',
        'before_title' => '<div class="recent-courses-title-wrapper">
                   <div class="recent-courses-title-head">
                        <h3 class="recent-courses-title">',
        'after_title' => '</h3></div></div>'
    ));

//    register_sidebars(1, array(
//        'id' => 'banner-sidebar',
//        'name' => __('Banner sidebar', 'sg'),
//        'description' => __('sidebar for home-page', 'sg'),
//        'class' => '',
//        'before_widget' => '<div class="popular-courses">
//                <div class="container">',
//        'after_widget' => '</div></div>',
//        'before_title' => '<div class="popular-courses-title-wrapper">
//                    <div class="popular-courses-title-head">
//                        <h3 class="popular-courses-title">',
//        'after_title' => '</h3></div></div>'
//    ));
//
//    register_sidebars(1, array(
//        'id' => 'testimonial-sidebar',
//        'name' => __('Testimonial sidebar', 'sg'),
//        'description' => __('sidebar for home-page', 'sg'),
//        'class' => 'row',
//        'before_widget' => '<div class="testimonial-wrapper">
//                <div class="container">',
//        'after_widget' => '</div></div>',
//        'before_title' => '<div class="popular-courses-title-wrapper">
//                    <div class="popular-courses-title-head">
//                        <h3 class="popular-courses-title">',
//        'after_title' => '</h3></div></div>'
//    ));
//
//    register_sidebars(1, array(
//        'id' => 'contact-form-sidebar',
//        'name' => __('contact form sidebar', 'sg'),
//        'description' => __('sidebar for contact form', 'sg'),
//        'class' => '',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '<h3 class="contact-title">',
//        'after_title' => '</h3>'
//    ));
//
//    register_sidebars(1, array(
//        'id' => 'contact-right-sidebar',
//        'name' => __('contact right sidebar', 'sg'),
//        'description' => __('sidebar for contacts', 'sg'),
//        'class' => '',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '<h3 class="contact-right">',
//        'after_title' => '</h3>'
//    ));
}
/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'photo', 900, 300, true );
    add_image_size( 'footer-widget-thumb', 70, 70, true );
    add_image_size( 'main-widget-thumb', 60, 60, true );
    add_image_size( 'instructor-thumb', 115, 115, true );
}
/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );
/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );
/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 200 );
/**
 * Підключаємо javascript файли
 */
function jquery_scripts_method() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
    wp_enqueue_script( 'jquery' );
}    

add_action( 'wp_enqueue_scripts', 'jquery_scripts_method', 11 );

function add_theme_scripts(){
    //wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    //wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery') );
    //wp_enqueue_script( 'slider', get_template_directory_uri() . '/js/slider.js', array('flexslider'), '', true );
    wp_enqueue_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDWnsioKO-xEyhJbMDHJL8SZcF7Ny8lPbw&v=3.exp&sensor=false');
    //wp_enqueue_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDWnsioKO-xEyhJbMDHJL8SZcF7Ny8lPbw&callback=initMap');
    wp_enqueue_script( 'googlemap', get_template_directory_uri() . '/js/googlemap.js', array('googleapis'), '', true );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
    wp_enqueue_script( 'rangeSlider', get_template_directory_uri() . '/js/ion.rangeSlider.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style( 'rangeSlider', get_template_directory_uri() . '/css/ion.rangeSlider.css');
    wp_enqueue_style( 'skinRangeSlider', get_template_directory_uri() . '/css/ion.rangeSlider.skinFlat.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:400,600,700');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

function reg_courses_tax(){
    register_taxonomy('estate_type', 'Buildings', array(
        'hierarchical' => true,
        'labels'                => array(
            'name'              => __('types', 'realEstate'),
            'singular_name'     => __('type', 'realEstate'),
        ),
        'show_ui' => true,
        'query_var' => true,
    ));
    register_taxonomy('estate_rooms', 'Buildings', array(
        'hierarchical' => true,
        'labels'                => array(
            'name'              => __('rooms', 'realEstate'),
            'singular_name'     => __('room', 'realEstate'),
        ),
        'show_ui' => true,
        'query_var' => true,
    ));
    register_taxonomy('estate_district', 'Buildings', array(
        'hierarchical' => true,
        'labels'                => array(
            'name'              => __('districts', 'realEstate'),
            'singular_name'     => __('district', 'realEstate'),
        ),
        'show_ui' => true,
        'query_var' => true,
    ));
}

add_action('init', 'reg_courses_tax');

function register_posts() {

    /**
     * Post Type: Building.
     */

    $labels = array(
        "name" => __( 'Buildings', 'realEstate' ),
        "singular_name" => __( 'Building', 'realEstate' ),
        "add_new" => __( 'add new', 'realEstate' ),
        "all_items" => __( 'All Buildings', 'realEstate' ),
    );

    $args = array(
        "label" => __( 'Buildings', 'realEstate' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "quick_info", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => 'dashicons-admin-multisite',
        "taxonomies" => array( 'estate_type', 'estate_rooms', 'estate_district' ),
        "supports" => array( "title", "editor", "thumbnail", "excerpt" ),
    );

    register_post_type( "Buildings", $args );

    /**
     * Post Type: Slider.
     */

    $labels = array(
        "name" => __( 'Sliders', 'realEstate' ),
        "singular_name" => __( 'Slider', 'realEstate' ),
        "add_new" => __( 'add new', 'realEstate' ),
        "all_items" => __( 'All Sliders', 'realEstate' ),
    );

    $args = array(
        "label" => __( 'Sliders', 'realEstate' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "quick_info", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => 'dashicons-images-alt2',
        "supports" => array( "title", "editor", "thumbnail", "excerpt" ),
    );

    register_post_type( "slider", $args );

    /**
     * Post Type: News.
     */

    $labels = array(
        "name" => __( 'News', 'realEstate' ),
        "singular_name" => __( 'news', 'realEstate' ),
        "add_new" => __( 'add new', 'realEstate' ),
        "all_items" => __( 'All News', 'realEstate' ),
    );

    $args = array(
        "label" => __( 'News', 'realEstate' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "quick_info", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => 'dashicons-megaphone',
        "supports" => array( "title", "editor", "thumbnail", "excerpt" ),
    );

    register_post_type( "news", $args );


//    /**
//     * Post Type: courses.
//     */
//
//    $labels = array(
//        "name" => __( 'courses', 'sg' ),
//        "singular_name" => __( 'course', 'sg' ),
//    );
//
//    $args = array(
//        "label" => __( 'courses', 'sg' ),
//        "labels" => $labels,
//        "description" => "",
//        "public" => true,
//        "publicly_queryable" => true,
//        "show_ui" => true,
//        "show_in_rest" => false,
//        "rest_base" => "",
//        "has_archive" => false,
//        "show_in_menu" => true,
//        "exclude_from_search" => false,
//        "capability_type" => "post",
//        "map_meta_cap" => true,
//        "hierarchical" => false,
//        "rewrite" => array( "slug" => "cl_course", "with_front" => true ),
//        "query_var" => true,
//        "supports" => array( "title", "thumbnail" ),
//    );
//
//    register_post_type( "cl_course", $args );
//
//    /**
//     * Post Type: instructors.
//     */
//
//    $labels = array(
//        "name" => __( 'instructors', 'sg' ),
//        "singular_name" => __( 'instructor', 'sg' ),
//    );
//
//    $args = array(
//        "label" => __( 'instructors', 'sg' ),
//        "labels" => $labels,
//        "description" => "",
//        "public" => true,
//        "publicly_queryable" => true,
//        "show_ui" => true,
//        "show_in_rest" => false,
//        "rest_base" => "",
//        "has_archive" => false,
//        "show_in_menu" => true,
//        "exclude_from_search" => false,
//        "capability_type" => "post",
//        "map_meta_cap" => true,
//        "hierarchical" => false,
//        "rewrite" => array( "slug" => "cl_instructor", "with_front" => true ),
//        "query_var" => true,
//        "supports" => array( "title", "editor", "thumbnail" ),
//    );
//
//    register_post_type( "cl_instructor", $args );
//
//    /**
//     * Post Type: testimonials.
//     */
//
//    $labels = array(
//        "name" => __( 'testimonials', 'sg' ),
//        "singular_name" => __( 'testimonial', 'sg' ),
//    );
//
//    $args = array(
//        "label" => __( 'testimonials', 'sg' ),
//        "labels" => $labels,
//        "description" => "",
//        "public" => true,
//        "publicly_queryable" => true,
//        "show_ui" => true,
//        "show_in_rest" => false,
//        "rest_base" => "",
//        "has_archive" => false,
//        "show_in_menu" => true,
//        "exclude_from_search" => false,
//        "capability_type" => "post",
//        "map_meta_cap" => true,
//        "hierarchical" => false,
//        "rewrite" => array( "slug" => "testimonial", "with_front" => true ),
//        "query_var" => true,
//        "supports" => array( "title", "editor" ),
//    );
//
//    register_post_type( "testimonial", $args );
}

add_action( 'init', 'register_posts' );

/*
Добавляемо додаткові поля до типу зипису Buildings
*/

add_action('add_meta_boxes', 'metaprice_init'); 
add_action('save_post', 'metaprice_save'); 

function metaprice_init() { 
add_meta_box('metaprice', 'Characteristics', 'metaprice_show', 'Buildings', 'normal', 'default'); 
} 
//Функция рисования метабокса:
function metaprice_show($post, $box) { 
  //получение существующих метаданных
$pricedata = get_post_meta($post->ID, '_metaprice_data', true); 
$squaredata = get_post_meta($post->ID, '_metasquare_data', true); 
$floordata = get_post_meta($post->ID, '_metafloor_data', true); 
  // скрытое поле с одноразовым кодом 
wp_nonce_field('metaprice_action', 'metaprice_nonce'); 
wp_nonce_field('metasquare_action', 'metasquare_nonce'); 
wp_nonce_field('metafloor_action', 'metafloor_nonce'); 
  // поле с метаданными 
echo '<p>Цена: <input type="text" name="metadata_price_field" value="' . esc_attr($pricedata) . '"/></p>'; 
echo '<p>Площадь (общ./жил./кух.): <input type="text" name="metadata_square_field" value="' . esc_attr($squaredata) . '"/></p>'; 
echo '<p>Этаж/этажность: <input type="text" name="metadata_floor_field" value="' . esc_attr($floordata) . '"/></p>'; 
} 
//функция сохранения данных:
function metaprice_save($postID) { 
  // пришло ли поле наших данных? 
if (!isset($_POST['metadata_price_field']))
return; 
if (!isset($_POST['metadata_square_field']))
return;
if (!isset($_POST['metadata_floor_field']))
return;
  // не происходит ли автосохранение? 
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
return; 
  // не ревизию ли сохраняем? 
if (wp_is_post_revision($postID)) 
return; 
  // проверка достоверности запроса 
check_admin_referer('metaprice_action', 'metaprice_nonce'); 
check_admin_referer('metasquare_action', 'metasquare_nonce'); 
check_admin_referer('metafloor_action', 'metafloor_nonce'); 
  // коррекция данных 
$pricedata = sanitize_text_field($_POST['metadata_price_field']); 
$squaredata = sanitize_text_field($_POST['metadata_square_field']); 
$floordata = sanitize_text_field($_POST['metadata_floor_field']); 
  // запись 
update_post_meta($postID, '_metaprice_data', $pricedata); 
update_post_meta($postID, '_metasquare_data', $squaredata); 
update_post_meta($postID, '_metafloor_data', $floordata); 
} 









class RecentPostsFooter extends WP_Widget
{
    public function __construct() {
        parent::__construct('RecentPostsFooter_widget', __('Recent posts for footer', 'sg'),
            array('description' => __('A simple widget to get recent posts for footer', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'category__in' => $instance["category"],
            'showposts' => $instance["count"],
        ) );

        if ( $the_query->have_posts() ){
            echo '<ul class="list-unstyled">';
            while( $the_query->have_posts() ){
                $the_query->the_post();
                ?>

                <div class="recent-post-widget-item clearfix">
                    <?php if ( has_post_thumbnail()) { ?>
                    <div class="recent-post-widget-thumbnail">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail('footer-widget-thumb'); ?>
                        </a>
                    </div>
                    <?php } ?>
                    <div class="recent-post-widget-item-content">
                        <div class="recent-post-widget-title">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="recent-post-widget-date">
                            <span>Posted on</span>
                            <a href="<?php the_permalink(); ?>"><?php the_time('j F Y'); ?></a>
                        </div>
                    </div>
                </div>

                <?php
            }
            echo '</ul>';

            wp_reset_postdata();
        }

        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["category"] = htmlentities( $newInstance["category"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $category = null;
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $category = $instance["category"];
            $count = $instance["count"];
        }

        $categories = get_categories();

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $categoryId = $this->get_field_id("category");
        $categoryName = $this->get_field_name("category");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $categoryId; ?>"><?php echo __('Category', 'sg'); ?></label>
            <select class="widefat" name="<?php echo $categoryName; ?>" id="<?php echo $categoryId; ?>">
                <?php foreach ($categories as $cat){ ?>
                    <option value="<?php echo $cat->cat_ID; ?>"<?php selected($category, $cat->cat_ID); ?>><?php echo $cat->name; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class RecentPostsFooter

add_action( "widgets_init", function () {
    register_widget( "RecentPostsFooter" );
});


class RecentNewsWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('RecentNews_widget', __('Recent News Widget', 'sg'),
            array('description' => __('A simple widget to get Recent News', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'category__in' => $instance["category"],
            'showposts' => $instance["count"],
        ) );

        if ( $the_query->have_posts() ){
            while( $the_query->have_posts() ){
                $the_query->the_post();
                ?>

                <div class="blog-item-wrapper row">
                     <div class="col-md-12">
                         <div class="blog-item clearfix">
                            <?php if ( has_post_thumbnail()) { ?>
                                <div class="blog-thumbnail">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                        <?php the_post_thumbnail('main-widget-thumb'); ?>
                                    </a>
                                </div>
                            <?php } ?>
                             <div class="post-header">
                                 <h3 class="blog-item-title">
                                     <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                 </h3>
                                 <div class="blog-item-date">
                                     <a href="<?php the_permalink(); ?>"><?php the_time('j F Y'); ?></a>
                                 </div>
                            </div>
                         </div>
                     </div>
                </div>

                <?php
            }

            wp_reset_postdata();
        }

        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["category"] = htmlentities( $newInstance["category"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $category = null;
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $category = $instance["category"];
            $count = $instance["count"];
        }

        $categories = get_categories();

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $categoryId = $this->get_field_id("category");
        $categoryName = $this->get_field_name("category");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $categoryId; ?>"><?php echo __('Category', 'sg'); ?></label>
            <select class="widefat" name="<?php echo $categoryName; ?>" id="<?php echo $categoryId; ?>">
                <?php foreach ($categories as $cat){ ?>
                    <option value="<?php echo $cat->cat_ID; ?>"<?php selected($category, $cat->cat_ID); ?>><?php echo $cat->name; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class RecentNewsWidget

add_action( "widgets_init", function () {
    register_widget( "RecentNewsWidget" );
});

class FeaturedInstructor extends WP_Widget
{
    public function __construct() {
        parent::__construct('FeaturedInstructor_widget', __('Featured Instructor Widget', 'sg'),
            array('description' => __('A simple widget to show Featured Instructor', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'p' => $instance["instructor"],
            'post_type' => 'cl_instructor',
        ) );

        if ( $the_query->have_posts() ){
            while( $the_query->have_posts() ){
                $the_query->the_post();
                ?>

                <div class="instructor-item-wrapper">
                    <div class="instructor-content">
                        <?php if ( has_post_thumbnail()) { ?>
                            <div class="instructor-thumbnail">
                                <?php the_post_thumbnail('instructor-thumb'); ?>
                            </div>
                        <?php } ?>
                        <div class="instructor-title-wrapper">
                            <h3 class="instructor-title"><?php the_title(); ?></h3>
                            <div class="instructor-position">
                                <?php echo get_post_meta($instance["instructor"], 'instructor_position', true); ?>
                            </div>
                        </div>
                        <div class="instructor-description">
                            <?php the_excerpt(); ?>
                        </div>
                        <a class="instructor-button" href="<?php the_permalink(); ?>">View profile</a>
                    </div>
                </div>

                <?php
            }
            wp_reset_postdata();
        }

        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["instructor"] = htmlentities( $newInstance["instructor"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $instructor = null;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $instructor = $instance["instructor"];
        }

        $instructors = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'cl_instructor',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ));

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $instructorId = $this->get_field_id("instructor");
        $instructorName = $this->get_field_name("instructor");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $instructorId; ?>"><?php echo __('Instructor', 'sg'); ?></label>
            <select class="widefat" name="<?php echo $instructorName; ?>" id="<?php echo $instructorId; ?>">
                <?php foreach ($instructors as $item){ ?>
                    <option value="<?php echo $item->ID; ?>"<?php selected($instructor, $item->ID); ?>><?php echo $item->post_title; ?></option>
                <?php } ?>
            </select>
        </p>
        <?php
    }
} // class FeaturedInstructor

add_action( "widgets_init", function () {
    register_widget( "FeaturedInstructor" );
});

class VideoNews extends WP_Widget
{
    public function __construct() {
        parent::__construct('VideoNews_widget', __('VideoNews Widget', 'sg'),
            array('description' => __('A simple widget to show Video News', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        ?>

        <div class="content-item-wrapper">
            <p>
                <iframe src="<?php echo $instance['link']?>" width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </p>
            <h4><?php echo $instance['caption']?></h4>
            <p><?php echo $instance['text']?></p>
        </div>

        <?php
        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["text"] = htmlentities( $newInstance["text"] );
        $values["link"] = htmlentities( $newInstance["link"] );
        $values["caption"] = htmlentities( $newInstance["caption"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $text = "";
        $link ="";
        $caption ="";

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $text = $instance["text"];
            $link = $instance["link"];
            $caption = $instance["caption"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $textId = $this->get_field_id("text");
        $textName = $this->get_field_name("text");

        $linkId = $this->get_field_id("link");
        $linkName = $this->get_field_name("link");

        $captionId = $this->get_field_id("caption");
        $captionName = $this->get_field_name("caption");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $captionId; ?>"><?php echo __('Caption', 'sg'); ?></label>
            <textarea class="widefat" id="<?php echo $captionId; ?>" name="<?php echo $captionName; ?>" ><?php echo $caption; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $textId; ?>"><?php echo __('Text', 'sg'); ?></label>
            <textarea class="widefat" id="<?php echo $textId; ?>" name="<?php echo $textName; ?>" ><?php echo $text; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $linkId; ?>"><?php echo __('Link', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $linkId; ?>" name="<?php echo $linkName; ?>" type="text" value="<?php echo $link; ?>">
        </p>

        <?php
    }
} // class VideoNews

add_action( "widgets_init", function () {
    register_widget( "VideoNews" );
});

class RecentCoursesWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('RecentCourses_widget', __('Recent Courses Widget', 'sg'),
            array('description' => __('A simple widget to get Recent Courses', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'showposts' => $instance["count"],
            'orderby' => 'date',
            'post_type' => 'cl_course',
            'post_status' => 'publish',
        ) );

        if ( $the_query->have_posts() ){ ?>
            <div class="flexslider">
                <ul class="slides">
            <?php
            while( $the_query->have_posts() ){
                $the_query->the_post();
                ?>
                <li>
                    <div class="course-item">
                        <?php if ( has_post_thumbnail()) { ?>
                            <div class="course-image">
                                <a href="<?php the_permalink()?>"><?php the_post_thumbnail(); ?></a>
                            </div>
                        <?php } ?>
                        <div class="course-content-wrapper clearfix">
                            <h3 class="course-title">
                                <a href="<?php the_permalink()?>"><?php the_title()?></a>
                            </h3>
                            <div class="course-price">
                                <span class="price-button">
                                    <?php echo '$' . get_post_meta(get_the_ID(), 'cc_price', true); ?>
                                </span>
                            </div>
                            <div class="course-info">
                                <span  class="glyphicon glyphicon-time"></span>
                                <?php
                                $tmp_date = get_post_meta(get_the_ID(), 'cc_date_begin', true);
                                $timestamp = strtotime($tmp_date);
                                $date_begin = date('j F Y', $timestamp + (get_option( 'gmt_offset' ) * HOUR_IN_SECONDS));
                                $tmp_date = get_post_meta(get_the_ID(), 'cc_date_end', true);
                                $timestamp = strtotime($tmp_date);
                                $date_end = date('j F Y', $timestamp + (get_option( 'gmt_offset' ) * HOUR_IN_SECONDS));
                                ?>
                                <span class="date"><?php echo $date_begin . '-' . $date_end; ?></span>
                            </div>
                        </div>
                    </div>
                </li>

                <?php
            }
            ?> </ul>
            </div>
            <?php

            wp_reset_postdata();
        }

        echo $args['after_widget'];

    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $count = $instance["count"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>

        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class RecentCoursesWidget

add_action( "widgets_init", function () {
    register_widget( "RecentCoursesWidget" );
});

class PopularCoursesWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('PopularCourses_widget', __('Popular Courses Widget', 'sg'),
            array('description' => __('A simple widget to get Popular Courses', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] );
            ?>
            <div class="popular-courses-title-devider"></div>
            <div class="popular-courses-title-caption">
                <?php echo $instance['caption']; ?>
            </div>
            <?php
            echo $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'showposts' => $instance["count"],
            'orderby' => 'date',
            'post_type' => 'cl_course',
            'post_status' => 'publish',
        ) );

        if ( $the_query->have_posts() ){ ?>
            <div class="flexslider">
                <ul class="slides">
                    <?php
                    while( $the_query->have_posts() ){
                        $the_query->the_post();
                        ?>
                        <li>
                            <div class="course-item">
                                <?php if ( has_post_thumbnail()) { ?>
                                    <div class="course-image">
                                        <a href="<?php the_permalink()?>"><?php the_post_thumbnail(); ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                        </li>

                        <?php
                    }
                    ?> </ul>
            </div>
            <?php

            wp_reset_postdata();
        }

        echo $args['after_widget'];

    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["caption"] = htmlentities( $newInstance["caption"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $caption = "";
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $caption = $instance["caption"];
            $count = $instance["count"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $captionId = $this->get_field_id("caption");
        $captionName = $this->get_field_name("caption");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>

        <p>
            <label for="<?php echo $captionId; ?>"><?php echo __('Caption', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $captionId; ?>" name="<?php echo $captionName; ?>" type="text" value="<?php echo $caption; ?>">
        </p>

        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class PopularCoursesWidget

add_action( "widgets_init", function () {
    register_widget( "PopularCoursesWidget" );
});

class TestimonialsWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('Testimonials_widget', __('Testimonials Widget', 'sg'),
            array('description' => __('A simple widget to get Testimonials', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] );
            ?>
            <div class="popular-courses-title-devider"></div>
            <?php
            echo $args['after_title'];
        }

        $the_query = new WP_Query( array (
            'showposts' => $instance["count"],
            'orderby' => 'date',
            'post_type' => 'testimonial',
            'post_status' => 'publish',
        ) );

        if ( $the_query->have_posts() ){ ?>
            <div class="flexslider-t">
                <ul class="slides">
                    <?php
                    while( $the_query->have_posts() ){
                        $the_query->the_post();
                        ?>
                        <li>
                            <div class="course-item">
                                <p><?php the_content()?></p>
                            </div>
                            <span><?php the_title()?></span><span><?php echo ', ' . get_post_meta(get_the_ID(), 'testimonial_pos', true) ?></span>
                        </li>

                        <?php
                    }
                    ?> </ul>
            </div>
            <?php

            wp_reset_postdata();
        }

        echo $args['after_widget'];

    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $count = $instance["count"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>

        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class TestimonialsWidget

add_action( "widgets_init", function () {
    register_widget( "TestimonialsWidget" );
});

class HeaderWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('Header_widget', __('Header Widget', 'sg'),
            array('description' => __('A simple widget to get Header', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget']; ?>

        <ul class="list-unstyled">
            <li class="text-right">
                <span class="glyphicon glyphicon-earphone phone"></span><span><?php echo $instance['phone']; ?></span>
            </li>
            <li class="text-right">
                <!-- <span class="glyphicon glyphicon-envelope"></span> --><span><?php echo $instance['email']; ?></span>
            </li>
        </ul>
                <!-- <ul class="nav navbar-nav navbar-right">
                    <li>
                        <span class="navbar-text glyphicon glyphicon-lock"></span>
                    </li>
                    <li>
                        <a href="#">Sign In</a>
                    </li>
                    <li>
                        <a href="#">Sign Up</a>
                    </li>
                </ul> -->

    <?php echo $args['after_widget'];

    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["phone"] = htmlentities( $newInstance["phone"] );
        $values["email"] = htmlentities( $newInstance["email"] );
        return $values;
    }

    public function form( $instance ) {
        $phone = "";
        $email = "";

        if ( ! empty( $instance ) ) {
            $phone = $instance["phone"];
            $email = $instance["email"];
        }

        $phoneId = $this->get_field_id("phone");
        $phoneName = $this->get_field_name("phone");

        $emailId = $this->get_field_id("email");
        $emailName = $this->get_field_name("email");

        ?>
        <p>
            <label for="<?php echo $phoneId; ?>"><?php echo __('Phone', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $phoneId; ?>" name="<?php echo $phoneName; ?>" type="text" value="<?php echo $phone; ?>">
        </p>

        <p>
            <label for="<?php echo $emailId; ?>"><?php echo __('Email', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $emailId; ?>" name="<?php echo $emailName; ?>" type="text" value="<?php echo $email; ?>">
        </p>

        <?php
    }
} // class HeaderWidget

add_action( "widgets_init", function () {
    register_widget( "HeaderWidget" );
});

class ContactWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('Contact_widget', __('Contact Widget', 'sg'),
            array('description' => __('A simple widget to get Contact', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] );
            echo $args['after_title'];
        } ?>

            <p>
                <span><?php echo $instance['address']; ?></span>
            </p>
            <p>
                <span class="glyphicon glyphicon-earphone"></span><span><?php echo $instance['phone']; ?></span>
            </p>
            <p>
                <span class="glyphicon glyphicon-envelope"></span><span><?php echo $instance['email']; ?></span>
            </p>
            <p>
                <span><?php echo $instance['time']; ?></span>
            </p>

        <?php echo $args['after_widget'];

    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["phone"] = htmlentities( $newInstance["phone"] );
        $values["email"] = htmlentities( $newInstance["email"] );
        $values["address"] = htmlentities( $newInstance["address"] );
        $values["time"] = htmlentities( $newInstance["time"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $phone = "";
        $email = "";
        $address = "";
        $time = "";

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $phone = $instance["phone"];
            $email = $instance["email"];
            $address = $instance["address"];
            $time = $instance["time"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $phoneId = $this->get_field_id("phone");
        $phoneName = $this->get_field_name("phone");

        $emailId = $this->get_field_id("email");
        $emailName = $this->get_field_name("email");

        $addressId = $this->get_field_id("address");
        $addressName = $this->get_field_name("address");

        $timeId = $this->get_field_id("time");
        $timeName = $this->get_field_name("time");

        ?>

        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>

        <p>
            <label for="<?php echo $addressId; ?>"><?php echo __('Address', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $addressId; ?>" name="<?php echo $addressName; ?>" type="text" value="<?php echo $address; ?>">
        </p>

        <p>
            <label for="<?php echo $phoneId; ?>"><?php echo __('Phone', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $phoneId; ?>" name="<?php echo $phoneName; ?>" type="text" value="<?php echo $phone; ?>">
        </p>

        <p>
            <label for="<?php echo $emailId; ?>"><?php echo __('Email', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $emailId; ?>" name="<?php echo $emailName; ?>" type="text" value="<?php echo $email; ?>">
        </p>

        <p>
            <label for="<?php echo $timeId; ?>"><?php echo __('Time', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $timeId; ?>" name="<?php echo $timeName; ?>" type="text" value="<?php echo $time; ?>">
        </p>

        <?php
    }
} // class ContactWidget

add_action( "widgets_init", function () {
    register_widget( "ContactWidget" );
});

function search_filter($query) {
    if ( ! is_admin() && $query->is_main_query() ) {
        if ($query->is_search) {
            if (!empty($_GET['post_type'])){
                $query->set('post_type', $_GET["post_type"]);
            }
            if (!empty($_GET['course_type'])) {
                $query->set( 'cl_course_type', $_GET["course_type"] );
            }
            if (!empty($_GET['course_cat'])) {
                $query->set( 'cl_course_cat', $_GET["course_cat"] );
            }
            return $query;
        }
    }
}

add_action( 'pre_get_posts', 'search_filter' );

function my_acf_google_map_api( $api ){

    $api['key'] = 'AIzaSyDWnsioKO-xEyhJbMDHJL8SZcF7Ny8lPbw';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');



/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['Buildings']) || isset( $_POST['Buildings'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * Nonce verification
	 */
	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );