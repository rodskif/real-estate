<!-- Footer -->

<footer style="background-color: #333">
  <div class="container">
      <div class="row">
        <div class="col-md-2">
            <div class="row">
                <div class="col-md-12">
                  <?php dynamic_sidebar('footer-logo'); ?>
                  <img src="<?php echo get_template_directory_uri() ?>/img/KG.jpg" alt="">
                </div>
                <div class="col-md-12">
                  <a class="social" href=""><i class="fa fa-vk fa-lg" aria-hidden="true"></i></a>
                  <a class="social" href=""><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                  <a class="social" href=""><i class="fa fa-linkedin fa-lg" aria-hidden="true"></i></a>
                  <a class="social" href=""><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                        <li><i class="fa-li fa fa-caret-right text-danger"></i>Однокомнатная квартира</li>
                    </ul>
                </div>
            </div>
          <?php dynamic_sidebar('footer-list'); ?>
        </div>
        <div class="col-md-4">
          <?php dynamic_sidebar('footer-item'); ?>
          <form>
            <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>              
            <input type="text" class="form-control" placeholder="Ваше имя">
            </div>
            <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-mobile fa-lg" aria-hidden="true"></i></span>           
            <input type="email" class="form-control" placeholder="Ваш е-mail">
            </div>
            <button type="submit" class="btn btn-danger btn-block">Підписатися</button>
          </form>
        </div>
      </div>
  </div>
    <div class="row" style="background-color: black; text-align: center;">
        <div class="col-lg-12">
            <p>Copyright © Melnik Yura <?php echo date('Y'); ?></p>
        </div>
    </div>  
</footer>


<? wp_footer(); ?>

</body>



</html>