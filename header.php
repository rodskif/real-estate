<!DOCTYPE>
<html>
    <head>
        <title><?php wp_title(''); ?> <?php bloginfo( 'name' ); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
<div class="container">
  <p class="pull-right"><span class="glyphicon glyphicon-user"></span><span><?php wp_loginout();?></span></p>
</div>
    <header id="header">
<!--      <div class="head-item"></div>-->
        <nav class="navbar navbar-default main-nav">
              
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand logo" href="<?php echo home_url(); ?>">
                <?php if( $logo = get_custom_logo() ){
                    echo $logo;
                } else { ?>
                    <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="logo">
                <?php } ?>
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <?php wp_nav_menu( array(
                'theme_location'  => 'primary',
                'menu'  => 'Main menu',
                'container'  => 'div',
                'container_class'  => 'collapse navbar-collapse',
                'container_id'  => 'bs-example-navbar-collapse-1',
                'menu_class'  => 'nav navbar-nav',
            ));
            ?>

            <!-- <div class="nav navbar-nav navbar-right"> -->
                <?php
                    if ( is_active_sidebar('header-sidebar') ){
                        dynamic_sidebar('header-sidebar');
                    }
                ?>
            <!-- </div> -->

          </div><!-- /.container-fluid -->
        </nav>
      
      <div class="ism-slider" id="my-slider">
  <ol>
    <li>
      <img src="<?php echo get_template_directory_uri() ?>/img/pexels-photo-261187.png" alt="">
      <div class="ism-caption ism-caption-1">Твоя оселя</div>
      <div class="h2" data-delay='200'>Від мрії до реальності</div>
      <div class="f3" data-delay='200'>_____</div>
      <div class="f4" data-delay='200'>Сайт з нерухомості на Хмельничинні</div>
     
      <ul class="slide-item">
        <li class="decor-item"></li>
        <li class="ism-1 active">01</li>
        <li class="ism-2">02</li>
        <li class="ism-3">03</li>
        <li class="ism-4">04</li>
      </ul>
    </li>
    <li>
      <img src="<?php echo get_template_directory_uri() ?> /img/Rectangle 1.png" alt="">
      <div class="ism-caption ism-caption-1">Твоя оселя</div>
      <div class="h2" data-delay='200'>Від мрії до реальності</div>
      <div class="f3" data-delay='200'>_____</div>
      <div class="f4" data-delay='200'>Сайт з нерухомості на Хмельничинні</div>
     
      <ul class="slide-item">
        <li class="decor-item"></li>
        <li class="ism-1">01</li>
        <li class="ism-2 active">02</li>
        <li class="ism-3">03</li>
        <li class="ism-4">04</li>
      </ul>
    </li>
    <li>
      <img src="http://imageslidermaker.com/ism/image/slides/summer-192179_1280.jpg">
      <div class="ism-caption ism-caption-1">Твоя оселя</div>
      <div class="h2" data-delay='200'>Від мрії до реальності</div>
      <div class="f3" data-delay='200'>_____</div>
      <div class="f4" data-delay='200'>Сайт з нерухомості на Хмельничинні</div>
     
      <ul class="slide-item">
        <li class="decor-item"></li>
        <li class="ism-1">01</li>
        <li class="ism-2">02</li>
        <li class="ism-3 active">03</li>
        <li class="ism-4">04</li>
      </ul>
    </li>
    <li>
      <img src="http://imageslidermaker.com/ism/image/slides/city-690332_1280.jpg">
      <div class="ism-caption ism-caption-1">Твоя оселя</div>
      <div class="h2" data-delay='200'>Від мрії до реальності</div>
      <div class="f3" data-delay='200'>_____</div>
      <div class="f4" data-delay='200'>Сайт з нерухомості на Хмельничинні</div>
     
      <ul class="slide-item">
        <li class="decor-item"></li>
        <li class="ism-1">01</li>
        <li class="ism-2">02</li>
        <li class="ism-3">03</li>
        <li class="ism-4 active">04</li>
      </ul>
    </li>
  </ol>
</div>
    </header>