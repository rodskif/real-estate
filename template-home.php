<?php /* Template Name: Home */ ?>
<?php get_header(); ?>

<div class="widgets container">
  <div class="row">
    <div class="col-md-8">
      <form class="search-box" action="" method="post">
       <div class="row">
        <div class="col-md-6">
          <p class="label-item">Вид нерухомості</p>
          <select name="type" id="type" class="form-control">
              <option name="flat" value="flat">Квартира</option>
              <option name="house" value="house">Будинок</option>
              <option name="townhouse" value="townhouse">Таунхаус</option>
          </select>
          <p class="label-item">Кімнат</p>
          <select name="rooms" id="rooms" class="form-control">
              <option name="one" value="1">1</option>
              <option name="two" value="2">2</option>
              <option name="three" value="3">3</option>
              <option name="four" value="4">4</option>
              <option name="five" value="5">5</option>
          </select>
        </div>
        <div class="col-md-6">
          <p class="price">Ціна: </p>


          <div class="extra-controls">
              <input type="text" class="js-input-from text-control" value="0">
              <input type="text" class="js-input-to text-control" value="0">
          </div>

          <div class="range-slider">
              <input type="text" class="js-range-slider" value="">
          </div>

          <p class="label-item district">Район</p>
          <select name="district" id="district" class="form-control">
              <option name="any" value="any">Будь-який</option>
              <option name="ozerna" value="ozerna">Озерна</option>
              <option name="vistavka" value="vistavka">Виставка</option>
              <option name="rakovo" value="rakovo">Ракове</option>
              <option name="dubovo" value="dubovo">Дубово</option>
              <option name="center" value="center">Центр</option>
          </select>
          
        </div>
       </div>  
       <div class="row">
         <div class="col-md-12 search-item">
           <input type="submit" class="form-control btn-danger search-btn" value="Знайти пропозиції">
         </div>
       </div>
      </form>
    </div>
    <div class="col-md-4">
      <img src="<?php echo get_template_directory_uri() ?>/img/btn.png" alt="">
    </div>
  </div>
  <hr class="dash">
  <div class="row">
    <div class="col-md-12">
      <img src="<?php echo get_template_directory_uri() ?>/img/baner.png" alt="">
    </div>
  </div>
  <hr class="dash">

</div>

<script>
    var $range = $(".js-range-slider"),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 1000,
    max = 1000000,
    from = 0,
    to = 0;

    $range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    from: 1000,
    to: 1000000,
    onStart: updateInputs,
    onChange: updateInputs
    });
    instance = $range.data("ionRangeSlider");

    function updateInputs (data) {
    from = data.from;
    to = data.to;

    $inputFrom.prop("value", from);
    $inputTo.prop("value", to);
    }

    $inputFrom.on("input", function () {
    var val = $(this).prop("value");

    // validate
    if (val < min) {
    val = min;
    } else if (val > to) {
    val = to;
    }

    instance.update({
    from: val
    });
    });

    $inputTo.on("input", function () {
    var val = $(this).prop("value");

    // validate
    if (val < from) {
    val = from;
    } else if (val > max) {
    val = max;
    }

    instance.update({
    to: val
    });
    });
</script>
<?php get_template_part( 'loop-main' ); ?>

<div class="container text-center">
<nav aria-label="Page navigation">
  <ul class="pagination">    
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
  </ul>

  <ul class="pagination">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
      </a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
  </ul>
</nav>
</div>

<?php get_template_part( 'loop-news' ); ?>

<?php get_footer(); ?>